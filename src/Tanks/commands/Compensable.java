package Tanks.commands;

public interface Compensable {

    public void makeCompensation();
}
