package Tanks.commands;

public interface Command {

    public void execute() throws CommandException;
}
