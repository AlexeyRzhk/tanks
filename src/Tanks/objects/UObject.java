package Tanks.objects;

public interface UObject {

    public void set(String key, Object value);

    public Object get(String key);
}
