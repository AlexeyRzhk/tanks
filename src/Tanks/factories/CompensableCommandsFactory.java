package Tanks.factories;

import Tanks.objects.UObject;
import Tanks.commands.Command;
import Tanks.commands.CompensableBurnCommand;
import Tanks.commands.CompensableMoveCommand;

public class CompensableCommandsFactory implements CommandsFactory {


    @Override
    public Command createMoveCommand(UObject obj) {
        return new CompensableMoveCommand(obj);
    }

    @Override
    public Command createBurnCommand(UObject obj) {
        return new CompensableBurnCommand(obj);
    }
}
