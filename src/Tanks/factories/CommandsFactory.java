package Tanks.factories;

import Tanks.objects.UObject;
import Tanks.commands.Command;

public interface CommandsFactory {

    public Command createMoveCommand(UObject obj);

    public Command createBurnCommand(UObject obj);
}
